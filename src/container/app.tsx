import * as React from 'react'
import { ReactElement } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Nav } from '../components'
import { ContentContextInterface } from '../ts-assets'


const contentContextDefaultVal: ContentContextInterface = {}

export const ContentContext = React.createContext(contentContextDefaultVal)

function AppContainer(): ReactElement {
  const contentContextVal: ContentContextInterface = contentContextDefaultVal

  return (
    <div>
      <Router>
        <ContentContext.Provider value={contentContextVal}>
          <Nav />
          <main>
            <Switch>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </main>
        </ContentContext.Provider>
      </Router>
    </div>
  )
}

export default AppContainer
