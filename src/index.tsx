import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {
  Client, dedupExchange, fetchExchange, Provider,
} from 'urql'
import { cacheExchange } from '@urql/exchange-graphcache'
import ErrorBoundary from 'antd/es/alert/ErrorBoundary'
import AppContainer from './container/app'
import 'antd/dist/antd.css'
import { API_URL } from './config'

const cache = cacheExchange()

// urql Graphql Client
const client = new Client({
  url: API_URL,
  exchanges: [dedupExchange, cache, fetchExchange],
})

ReactDOM.render((
  <Provider value={client}>
    <ErrorBoundary>
      <AppContainer />
    </ErrorBoundary>
  </Provider>
), document.getElementById('root'))
