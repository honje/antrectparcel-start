import * as React from 'react'
import { ReactElement } from 'react'
import { Typography } from 'antd'

export function Home(): ReactElement {
  return (
    <div>
      <Typography.Title level={1}>Home</Typography.Title>
    </div>
  )
}
