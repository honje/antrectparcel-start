import * as React from 'react'
import { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { Menu } from 'antd'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export function Nav() {
  const location = useLocation()
  const [currentSite, setCurrentSite] = useState('')

  useEffect(() => {
    let newUrl = location.pathname.split('/')[1]
    if (newUrl === '') newUrl = 'home'
    setCurrentSite(newUrl)
  }, [location])

  return (
    <nav>
      <Menu selectedKeys={[currentSite]} mode="horizontal">
        <Menu.Item key="home">
          <Link to="/">
            <FontAwesomeIcon icon={faHome} />
            Home
          </Link>
        </Menu.Item>
      </Menu>
    </nav>
  )
}
